## Running the Project

Follow these steps to run the project locally:

1. Start PostgreSQL in Docker using Docker Compose from the file `/docker/compose-pg/docker-compose.yml`:

   ```bash
   docker-compose -f docker/compose-pg/docker-compose.yml up -d
   ```

2. Run the application using Maven:

   ```bash
   mvn spring-boot:run
   ```

3. Open your web browser and navigate to [http://localhost:8080/](http://localhost:8080/) to access the application.

## Shutting Down

To shut down the project and clean up resources, follow these steps:

1. Stop the running Spring Boot application.

2. Stop PostgreSQL in Docker using Docker Compose:

   ```bash
   docker-compose -f docker/compose-pg/docker-compose.yml down
   ```

These steps will gracefully shut down the application and PostgreSQL database in Docker.