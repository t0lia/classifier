package com.apozdniakov.classifier.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.Set;

@Repository
public interface VehicleRepository extends CrudRepository<Vehicle, Long> {
    Optional<Vehicle> findByName(String vehicle);

    @Query(value = "SELECT * FROM vehicle WHERE parent_id = ?1", nativeQuery = true)
    Set<Vehicle> findChildrenById(Long id);
}
