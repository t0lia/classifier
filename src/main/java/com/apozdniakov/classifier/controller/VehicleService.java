package com.apozdniakov.classifier.controller;

import com.apozdniakov.classifier.repository.Vehicle;

import java.util.Optional;
import java.util.Set;

public interface VehicleService {

    Optional<Vehicle> getByName(String name);

    Optional<Vehicle> getById(Long id);

    Set<Vehicle> getChildrenById(Long id);

    VehicleClassificationNodeDto getFullHierarchy();

}
