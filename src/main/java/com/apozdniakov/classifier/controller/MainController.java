package com.apozdniakov.classifier.controller;

import com.apozdniakov.classifier.repository.Vehicle;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;
import java.util.Set;

@RestController
@RequiredArgsConstructor
public class MainController {

    private final VehicleService vehicleService;

    // get by name
    @GetMapping("vehicle/{name}/name")
    public Optional<Vehicle> getVehicleByName(@PathVariable String name) {
        return vehicleService.getByName(name);
    }

    @GetMapping("vehicle/{id}")
    public Optional<Vehicle> getVehicleById(@PathVariable Long id) {
        return vehicleService.getById(id);
    }

    //    get children
    @GetMapping("vehicle/{id}/children")
    public Set<Vehicle> getChildrenById(@PathVariable Long id) {
        return vehicleService.getChildrenById(id);
    }

    // get full hierarchy
    @GetMapping("vehicle")
    public VehicleClassificationNodeDto getFullHierarchy() {
        return vehicleService.getFullHierarchy();
    }
}
