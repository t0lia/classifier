package com.apozdniakov.classifier.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class VehicleController {

    @GetMapping("/vehicle-tree")
    public String showVehicleTree() {
        return "vehicle-tree_2"; // This corresponds to the HTML file name (without the extension)
    }
}