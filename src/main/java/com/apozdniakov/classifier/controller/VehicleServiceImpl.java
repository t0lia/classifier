package com.apozdniakov.classifier.controller;

import com.apozdniakov.classifier.repository.Vehicle;
import com.apozdniakov.classifier.repository.VehicleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toSet;

@RequiredArgsConstructor
@Service
public class VehicleServiceImpl implements VehicleService {

    private final VehicleRepository repository;
    private final String ROOT_NAME = "Vehicle";

    @Override
    public Optional<Vehicle> getByName(String name) {
        return repository.findByName(name);
    }

    @Override
    public Optional<Vehicle> getById(Long id) {
        return repository.findById(id);
    }

    @Override
    public Set<Vehicle> getChildrenById(Long id) {
        return repository.findChildrenById(id);
    }

    @Override
    public VehicleClassificationNodeDto getFullHierarchy() {
        Vehicle root = repository.findByName(ROOT_NAME).orElseThrow(() -> new RuntimeException("Root not found"));
        return buildNode(root.getId());
    }

    private VehicleClassificationNodeDto buildNode(Long id) {
        Vehicle vehicle = repository.findById(id).get();

        Set<Vehicle> vehicleChildren = repository.findChildrenById(id);

        Set<VehicleClassificationNodeDto> children = vehicleChildren.stream()
                .map(Vehicle::getId)
                .map(this::buildNode)
                .collect(toSet());

        return new VehicleClassificationNodeDto(id, vehicle.getName(), children);
    }
}
