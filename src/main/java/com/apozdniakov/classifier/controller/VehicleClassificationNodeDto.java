package com.apozdniakov.classifier.controller;

import lombok.*;

import java.util.Set;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class VehicleClassificationNodeDto {
    private Long id;
    private String name;
    private Set<VehicleClassificationNodeDto> children;
}
