-- TRUNCATE table vehicle;
-- Insert the root node (Vehicle)
INSERT INTO vehicle (id, parent_id, name)
VALUES
    (1, NULL, 'Vehicle');

-- Insert Land Vehicles under Vehicle
INSERT INTO vehicle (id, parent_id, name)
VALUES
    (2, 1, 'Land Vehicles'),
    (3, 2, 'Motorcycles'),
    (4, 2, 'Passenger Cars'),
    (5, 4, 'Sedans'),
    (6, 4, 'Hatchbacks'),
    (7, 4, 'SUVs'),
    (8, 4, 'Coupes'),
    (9, 4, 'Convertibles'),
    (10, 2, 'Trucks'),
    (11, 10, 'Pickup Trucks'),
    (12, 10, 'Vans'),
    (13, 10, 'Commercial Trucks'),
    (14, 2, 'Bicycles'),
    (15, 2, 'Trains');

-- Insert Water Vehicles under Vehicle
INSERT INTO vehicle (id, parent_id, name)
VALUES
    (16, 1, 'Water Vehicles (Marine)'),
    (17, 16, 'Boats'),
    (18, 17, 'Sailboats'),
    (19, 17, 'Motorboats'),
    (20, 17, 'Yachts'),
    (21, 17, 'Fishing Boats'),
    (22, 17, 'Canoes/Kayaks'),
    (23, 16, 'Ships'),
    (24, 23, 'Cargo Ships'),
    (25, 23, 'Cruise Ships'),
    (26, 23, 'Naval Ships'),
    (27, 23, 'Submarines');

-- Insert Air Vehicles under Vehicle
INSERT INTO vehicle (id, parent_id, name)
VALUES
    (28, 1, 'Air Vehicles (Aircraft)'),
    (29, 28, 'Airplanes'),
    (30, 29, 'Commercial Airlines'),
    (31, 29, 'Private Planes'),
    (32, 29, 'Cargo Planes'),
    (33, 28, 'Helicopters'),
    (34, 28, 'Drones'),
    (35, 28, 'Gliders'),
    (36, 28, 'Balloons');

-- Insert Space Vehicles under Vehicle
INSERT INTO vehicle (id, parent_id, name)
VALUES
    (37, 1, 'Space Vehicles'),
    (38, 37, 'Space Shuttles'),
    (39, 37, 'Space Probes'),
    (40, 37, 'Satellites'),
    (41, 37, 'Space Stations');

-- Insert Off-Road and Specialty Vehicles under Vehicle
INSERT INTO vehicle (id, parent_id, name)
VALUES
    (42, 1, 'Off-Road and Specialty Vehicles'),
    (43, 42, 'Off-Road Vehicles'),
    (44, 43, 'ATVs (All-Terrain Vehicles)'),
    (45, 43, 'Dirt Bikes'),
    (46, 43, 'Dune Buggies'),
    (47, 42, 'Construction Vehicles'),
    (48, 47, 'Excavators'),
    (49, 47, 'Bulldozers'),
    (50, 47, 'Cranes'),
    (51, 42, 'Emergency Vehicles'),
    (52, 51, 'Police Cars'),
    (53, 51, 'Fire Trucks'),
    (54, 51, 'Ambulances');

-- Insert Personal Mobility under Vehicle
INSERT INTO vehicle (id, parent_id, name)
VALUES
    (55, 1, 'Personal Mobility'),
    (56, 55, 'Skateboards'),
    (57, 55, 'Scooters'),
    (58, 55, 'Rollerblades'),
    (59, 55, 'Electric Unicycles'),
    (60, 55, 'Segways');

-- Insert Other/Unconventional Vehicles under Vehicle
INSERT INTO vehicle (id, parent_id, name)
VALUES
    (61, 1, 'Other/Unconventional Vehicles'),
    (62, 61, 'Amphibious Vehicles'),
    (63, 61, 'Hovercraft'),
    (64, 61, 'Submersibles'),
    (65, 61, 'Rickshaws'),
    (66, 61, 'Hot Air Balloons'),
    (67, 61, 'Zeppelins');
